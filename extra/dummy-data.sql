\connect todo
INSERT INTO public.login (email,hash,name,lastlogin,salt) VALUES ('user@example.com',ENCODE(SHA256('1234'), 'hex'),'Tester',NOW(),'salty');
INSERT INTO public.categories (name,userid) VALUES ('TODO', 1);
INSERT INTO public.tasks (category,name) VALUES (1,'Coding');
INSERT INTO public.fake (id) VALUES (1);