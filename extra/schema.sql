DO
$do$
BEGIN
    IF NOT EXISTS(SELECT FROM pg_database WHERE datname = 'todo') THEN
        CREATE DATABASE todo WITH ENCODING = 'UTF8';
    END IF;
END
$do$;

\connect todo

CREATE TABLE IF NOT EXISTS public.login (
    id SERIAL PRIMARY KEY,
    email TEXT UNIQUE NOT NULL,
    hash TEXT NOT NULL,
    name TEXT NOT NULL,
    lastlogin TIMESTAMPTZ,
    createdat TIMESTAMPTZ DEFAULT NOW(),
    salt TEXT NOT NULL,
    CONSTRAINT valid_email CHECK (email ~* '^[A-Za-z0-9._%-öäü]+@[A-Za-z0-9-]+[.][A-Za-z]+$')
);

CREATE TABLE IF NOT EXISTS public.categories (
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(50) NOT NULL,
    userid INTEGER REFERENCES public.login(id) ON DELETE CASCADE,
    createdat TIMESTAMPTZ DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS public.tasks (
    id SERIAL PRIMARY KEY,
    category INTEGER REFERENCES public.categories(id) ON DELETE CASCADE,
    name TEXT,
    duedate DATE,
    done BOOLEAN DEFAULT false,
    createdat TIMESTAMPTZ DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS fake(id SERIAL PRIMARY KEY);

CREATE OR REPLACE FUNCTION public.passcheck(text, text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
BEGIN

	RETURN (SELECT EXISTS(SELECT 1 FROM public.login WHERE email=$1 AND password=$2));

END; $_$;

CREATE OR REPLACE FUNCTION public.usercheck(text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
BEGIN
	RETURN (SELECT EXISTS(SELECT 1 FROM public.login WHERE email=$1));
END; $_$;

CREATE TABLE IF NOT EXISTS public.sessions (
    id integer NOT NULL,
    email text NOT NULL,
    hash text DEFAULT md5((random())::text) NOT NULL,
    created timestamp(0) without time zone DEFAULT now() NOT NULL,
    permanent boolean DEFAULT false
);

CREATE SEQUENCE IF NOT EXISTS public.sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY public.sessions ALTER COLUMN id SET DEFAULT nextval('public.sessions_id_seq'::regclass);

ALTER TABLE ONLY public.sessions DROP CONSTRAINT IF EXISTS sessions_pkey;
ALTER TABLE ONLY public.sessions ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);

ALTER TABLE sessions DROP CONSTRAINT IF EXISTS sessions_email_fkey;
ALTER TABLE ONLY public.sessions ADD CONSTRAINT sessions_email_fkey FOREIGN KEY (email) REFERENCES public.login(email) ON UPDATE CASCADE ON DELETE CASCADE;


DROP FUNCTION IF EXISTS check_auth;
CREATE OR REPLACE FUNCTION public.check_auth(text, text) RETURNS json
    LANGUAGE plpgsql
    AS $_$
DECLARE ret json default null;
DECLARE authenticated bool default false;
begin
SELECT case when count(*) > 0 then true else false end into authenticated from login where "email" = $1 AND "hash" = $2;
CASE WHEN authenticated = true THEN
  INSERT INTO sessions (email) VALUES ($1);
  SELECT json_agg(json_build_object('hash',s.hash, 'email', l.email, 'name', l.name, 'id', l.id)) INTO ret FROM sessions s LEFT JOIN login l ON s.email=l.email where s.id = currval('sessions_id_seq');
ELSE
  NULL;
END CASE;
RETURN ret;
end;
$_$;

DROP TRIGGER IF EXISTS delete_older_sessions ON sessions CASCADE;
DROP TRIGGER IF EXISTS update_last_login ON sessions CASCADE;

CREATE OR REPLACE FUNCTION public.delete_older_sessions() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
        DELETE FROM sessions WHERE sessions.created <= NOW() - interval '30 days' AND sessions.email = new.email AND sessions.permanent = FALSE;
        RETURN NEW;
    END;
$$;

CREATE OR REPLACE FUNCTION public.update_last_login() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE login SET lastlogin = now() where email = new.email;
    RETURN NEW;
END;
$$;

CREATE TRIGGER delete_older_sessions AFTER INSERT ON public.sessions FOR EACH ROW EXECUTE PROCEDURE public.delete_older_sessions();

CREATE TRIGGER update_last_login AFTER INSERT ON public.sessions FOR EACH ROW EXECUTE PROCEDURE public.update_last_login();



CREATE OR REPLACE FUNCTION register(text,text,text,text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE id INT default null;
BEGIN
    INSERT INTO login (email,name,hash,salt) VALUES ($1,$2,$3,$4)  ON CONFLICT ON CONSTRAINT login_email_key DO NOTHING;
    SELECT login.id INTO id FROM login where email=$1;
    RETURN id;
END;
$_$;

CREATE OR REPLACE FUNCTION getsalt(text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE ret text default null;
BEGIN
    SELECT salt INTO ret FROM login WHERE email = $1;
    RETURN ret;
END;
$_$;

DROP FUNCTION IF EXISTS getthemes;
CREATE OR REPLACE FUNCTION getthemes(text,text,text) RETURNS json
LANGUAGE plpgsql
AS $_$
DECLARE ret json DEFAULT null;
DECLARE authenticated bool DEFAULT false;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $2 AND hash = $3;
    CASE WHEN authenticated = true THEN
        SELECT json_agg(row_to_json(a)) INTO ret FROM (SELECT name,id FROM categories WHERE userid=$1::int ORDER BY id) a;
    ELSE 
        NULL;
    END CASE;
    RETURN ret;
END;
$_$;

CREATE OR REPLACE FUNCTION checkiftheme(text,text,text,text) RETURNS bool
LANGUAGE plpgsql
AS $_$
DECLARE ret bool DEFAULT false;
DECLARE authenticated bool DEFAULT false;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $3 AND hash = $4;
    CASE WHEN authenticated = true THEN
        SELECT EXISTS(SELECT 1 FROM categories WHERE id=$2::int AND name = $1) INTO ret;
    ELSE 
        NULL;
    END CASE;
    RETURN ret;
END;
$_$;

CREATE OR REPLACE FUNCTION addtheme(text,text,text,text) RETURNS bool
LANGUAGE plpgsql
AS $_$
DECLARE authenticated bool DEFAULT false;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $3 AND hash = $4;
    CASE WHEN authenticated = true THEN
        INSERT INTO categories (name,userid) VALUES ($1,$2::int);
    ELSE 
        NULL;
    END CASE;
    RETURN authenticated;
END;
$_$;

CREATE OR REPLACE FUNCTION deletetheme(text,text,text,text) RETURNS bool
LANGUAGE plpgsql
AS $_$
DECLARE authenticated bool DEFAULT false;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $3 AND hash = $4;
    CASE WHEN authenticated = true THEN
        DELETE FROM categories WHERE id = $1::int AND userid = $2::int;
    ELSE 
        NULL;
    END CASE;
    RETURN authenticated;
END;
$_$;

CREATE OR REPLACE FUNCTION addtask(text,text,text,text,text) RETURNS bool
LANGUAGE plpgsql
AS $_$
DECLARE authenticated bool DEFAULT false;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $4 AND hash = $5;
    CASE WHEN authenticated = true THEN
        INSERT INTO tasks (name,category,duedate) VALUES($1,$2::int,$3::date);
    ELSE 
        NULL;
    END CASE;
    RETURN authenticated;
END;
$_$;

CREATE OR REPLACE FUNCTION gettasks(text,text,text) RETURNS json
LANGUAGE plpgsql
AS $_$
DECLARE authenticated bool DEFAULT false;
DECLARE ret json DEFAULT null;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $2 AND hash = $3;
    CASE WHEN authenticated = true THEN
        SELECT json_agg(row_to_json(a)) INTO ret FROM (SELECT name,id,duedate,done FROM tasks WHERE category=$1::int ORDER BY id) a;
    ELSE 
        NULL;
    END CASE;
    RETURN ret;
END;
$_$;

CREATE OR REPLACE FUNCTION setchecked(text,text,text,text) RETURNS bool
LANGUAGE plpgsql
AS $_$
DECLARE authenticated bool DEFAULT false;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $3 AND hash = $4;
    CASE WHEN authenticated = true THEN
        UPDATE tasks SET done = $2::bool WHERE id = $1::int; 
    ELSE 
        NULL;
    END CASE;
    RETURN authenticated;
END;
$_$;

CREATE OR REPLACE FUNCTION deletetask(text,text,text) RETURNS bool
LANGUAGE plpgsql
AS $_$
DECLARE authenticated bool DEFAULT false;
BEGIN
    SELECT CASE WHEN count(*) > 0 THEN true ELSE false END into authenticated FROM sessions WHERE email = $2 AND hash = $3;
    CASE WHEN authenticated = true THEN
        DELETE FROM tasks WHERE id = $1::int;
    ELSE 
        NULL;
    END CASE;
    RETURN authenticated;
END;
$_$;