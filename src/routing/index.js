const zazler = require('zazler');

/**
 * Configures Zazler API and returns Express middleware callback function.
 */
const setupZazlerApi = async () => {
    const zConf = {
        read: 'login tasks categories sessions fake',
        write: 'login tasks categories -login(id)',
        dbName: process.env.PG_DATABASE,
        inlineStrings: true,
        varsWithout$: true,
        protect: '* -fake',
        auth: {
            table: 'sessions',
            select: 'email',
            where: 'email=req.email:hash=req.hash',
            realm: 'Protected zone',
        },

        'write-rules': [/*
            { table: 'login', on: 'notnull(new.delete)', action: 'delete', where: 'id=new.id' },
            { table: 'login', on: 'notnull(new.insert)', action: 'insert', returning: 'seq id login_id_seq' },
            { table: 'categories', on: 'notnull(new.delete)', action: 'delete', where: 'id=new.id' },
            { table: 'categories', on: 'notnull(new.insert)', action: 'insert', returning: 'seq id categories_id_seq' },
            { table: 'tasks', on: 'notnull(new.delete)', action: 'delete', where: 'id=new.id' },
            { table: 'tasks', on: 'notnull(new.insert)', action: 'insert', returning: 'seq id tasks_id_seq' },
            { table: 'tasks', on: 'notnull(new.id)', action: 'update', where: 'id=new.id' },*/
        ],
    };

    const conn = {
        type: 'pg',
        host: process.env.PG_HOST,
        port: process.env.PG_PORT,
        database: process.env.PG_DATABASE,
        user: process.env.PG_USERNAME,
        password: process.env.PG_PASSWORD,
    };

    const zazlerApi = await zazler(conn, zConf);

    zazlerApi.setSqlFn({
        usercheck: 'usercheck($0)',
        passcheck: 'passcheck($0,$1)',
        check_auth: 'check_auth($0,$1)',
        register: 'register($0,$1,$2,$3)',
        getsalt: 'getsalt($0)',
        getthemes: 'getthemes($0,$1,$2)',
        checkiftheme: 'checkiftheme($0,$1,$2,$3)',
        addtheme: 'addtheme($0,$1,$2,$3)',
        deletetheme: 'deletetheme($0,$1,$2,$3)',
        addtask: 'addtask($0,$1,$2,$3,$4)',
        gettasks: 'gettasks($0,$1,$2)',
        setchecked: 'setchecked($0,$1,$2,$3)',
        deletetask: 'deletetask($0,$1,$2)',
    });

    return zazlerApi.expressRequest;
};

exports.mountRoutes = async (app) => {
    // All app routes
    app.use('/todo', await setupZazlerApi());
    app.get('/health', (req, res) => res.send('OK')); // Health check route
};
