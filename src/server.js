const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const { mountRoutes } = require('./routing');

process.on('uncaughtException', (err) => console.error(err)); // Unhandled exceptions
process.on('unhandledRejection', (err) => console.error(err)); // Unhandled Promise rejections

dotenv.config(); // Loads .env file contents into process.env.
const PORT = process.env.SERVER_PORT;

async function main() {
    const app = express();
    app.use(morgan('dev')); // Requests logger middleware
    await mountRoutes(app);
    app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
}

main().catch((err) => console.error(`Server failed to start with error: ${JSON.stringify(err)}`));
