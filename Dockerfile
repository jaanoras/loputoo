# https://hub.docker.com/r/library/node/tags/
FROM node:12.15.0-alpine

# Optional (install bash and nano)
RUN apk --no-cache add \
      bash \
      nano

# https://github.com/remy/nodemon/releases
RUN npm install --no-optional --global nodemon

# Create app directory
WORKDIR /app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Install app dependencies
RUN npm install
