# VIKK TAK18 Jaan Oras lõputöö - TODO rakenduse API

## API

Käesolev rakendus kasutab andmete serveerimiseks [Zazler](https://github.com/kaiko/zazler) teeki. 

## Lokaalne arendus kasutades Dockerit

Rakendus koosneb NodeJS API serverist ning PostgreSQL andmebaasi serverist.  

Järgnevates näidetes on kasutusel [docker-compose](https://docs.docker.com/compose/reference/up/) käsud.  

Eelnimetatud serverite käivitamine (seiskamine `CTRL + C`)  

```bash
docker-compose up
```

**NB!** kui ei soovi konteinerite logi terminalis näha, saab konteinerid käivitada ka taustal (seiskamine `docker-compose stop`)  

```bash
docker-compose up -d
```

Servereid saab käivitada ka individuaalselt:

1. PostgreSQL serveri käivitamiseks `docker-compose up postgres`
2. NodeJS API serveri käivitamiseks `docker-compose up api_server`  
NB! kuna API server on konfigureeritud sõltuvaks andmebaasist, siis käivitatakse ka PostgreSQL server.

Konfigureeritud URL-d:  

1. NodeJS API server `localhost:8080`  
2. PostgreSQL server `localhost:5432`  

**NB!** Kui rakendusele lisandub teekide sõltuvusi `package.json` faili, siis on vajalik konteinerite uuesti ehitamine.  

Selleks seiska töötavad konteinerid ja ehita need uuesti:  

```bash
docker-compose up --build
```

Konteinerite terminalsessioonide käivitamine:

Postgres andmebaasiserver  
```bash
docker exec -it postgres_server_todo_app bash
```

API server
```bash
docker exec -it api_server_todo_app bash
```  

### Andmebaas

Dockeris loodud andmebaasiserveri andmebaasis puudub `todo` andmebaas koos vajalike tabelitega.  
API korrektseks testimiseks on vaja luua andmebaas koos tabelitega.    

Selleks tuleb andmebaas üles ehitada [schema.sql](./extra/schema.sql) faili abil.

```bash
cat extra/schema.sql | docker exec -i postgres_server_todo_app psql -U postgres
```  

Testandmete sisestamine [dummy-data.sql](./extra/dummy-data.sql) faili abil.

```bash
cat extra/dummy-data.sql | docker exec -i postgres_server_todo_app psql -U postgres
``` 
**NB!**  
Eelnevates näidetes on kasutatud `cat` käsku, mis puudub Windows `CMD` ja `Powershell` terminalides.  

Selleks, et eelnimetatud käske Windowsis probleemideta käivitada on tarvis installeerida terminal, mis enamkasutatud Linux käske käivitada suudab.  

Näiteks `Git Bash`, mis tuleb kaasa kui installeerida [Git SCM](https://git-scm.com/download/win).

Alternatiiv oleks kasutada [docker exec](https://docs.docker.com/engine/reference/commandline/exec/) käsku:  

```bash
docker exec -i postgres_server_todo_app psql -U postgres -d todo < ./extra/dummy-data.sql
```

Eelnimetatud andmebaasi üles seadmist `.sql` failide abil peab tegema vaid esimene kord kui PostgreSQL konteiner on esmakordselt loodud.

Kõigi loodud konteinerite ja andmete eemaldamiseks kasuta [docker-compose down](https://docs.docker.com/compose/reference/down/) käsku.

### Docker PostgreSQL ja psql

`psql` on PostgreSQL käsurea utiliit. Dockeri konteineris `psql` kasutamine:  

```bash
docker exec -it postgres_server_todo_app bash -c "psql -d todo -U postgres"
```

### Debuggeri / siluri kasutamine

NodeJS API käivitatakse [nodemon](https://nodemon.io/) abil.  
Nodemon kuulab muudatusi lähtekoodi failides ja käivitab koheselt NodeJS protsessi uuesti kui muudatused tuvastatakse.  

Silurit saab kasutada pordil **9229**.  
Kiireim viis siluri kasutamiseks on kasutada ***Google Chrome dedicated DevTools for Node.js*** [tööriista](https://youtu.be/i0GoIC7lE-A).

## Lokaalne arendus (Dockerita)

1. Sea üles andmebaas.
2. Konfigureeri rakendus kasutades [.env](./.env) faili.  
Antud väärtused laetakse [dotenv](https://www.npmjs.com/package/dotenv) teegi abil rakendusse.
3. Käivita rakendus `npm run dev:start`  

Muudatusi kuulatakse `nodemon` abil ja silur on kasutatav pordil **9229**.
